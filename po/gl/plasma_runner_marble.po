# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Xosé <xosecalvo@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:24+0000\n"
"PO-Revision-Date: 2011-11-15 00:17+0100\n"
"Last-Translator: Xosé <xosecalvo@gmail.com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: plasmarunner.cpp:36
#, kde-format
msgid "Shows the coordinates :q: in OpenStreetMap with Marble."
msgstr "Amosa as coordenadas :q: en OpenStreetMap con Marble."

#: plasmarunner.cpp:37
#, kde-format
msgid "Shows the geo bookmark containing :q: in OpenStreetMap with Marble."
msgstr ""
"Amosa o marcador xeográfico que conteña :q: en OpenStreetMap con Marble."

#: plasmarunner.cpp:58
#, kde-format
msgid "Show the coordinates %1 in OpenStreetMap with Marble"
msgstr "Amosar as coordenadas %1 en OpenStreetMap con Marble."

#: plasmarunner.cpp:124
#, kde-format
msgid "Show in OpenStreetMap with Marble"
msgstr "Amosar en OpenStreetMap con Marble."
